package core;

import java.io.*;
import java.util.zip.InflaterInputStream;


public class GitBlobObject {

	/*public static void main (String [] args) throws IOException{
		GitBlobObject bo = new GitBlobObject("sample_repos/sample01","4452771b4a695592a82313e3253f5e073e6ead8c");
	}*/

    private String type = "";
    private int length = 0;
    private byte[] content = null;
    private String path = "";
    private String fileContent = "";
    private String hash = "";

    //Constructor
    public GitBlobObject(String path, String hash) throws IOException{

        //Get path of the object
        this.path = path + File.separator + "objects" + File.separator + hash.substring(0, 2) + File.separator + hash.substring(2);

        this.hash = hash;

        //Get file content
        //String fileContent = "";

        try {
            // Read the inflated file
            InflaterInputStream iir = new InflaterInputStream(new FileInputStream(new File(this.path)));

            //We go to inflate all the content

            int lenTmp;

            StringBuffer sb = new StringBuffer();

            /* << We read until the value is different from \0 and -1. In this first part we read the header. After that (after \0) we will read the content >> */
            while ((lenTmp = iir.read()) != -1 && lenTmp != '\0'){
                sb.append((char) lenTmp);
            }
            //Prima lavoriamo sull'header, andando a recuperare tipo e dimensione
            String[] header = sb.toString().split(" ");
            this.type = header[0];
            this.length = Integer.parseInt(header[1]);

            /* << Using the content's dimension we control the it is equals exactly to the length >> */

            //We store the content as an array of bytes
            this.content = new byte[this.length];

            //We verify that the quantity of bytes is exactly the same of content length (with an assert condition).
            lenTmp = iir.read(this.content);
            assert(lenTmp == this.length);

            //Finally we close the reader
            iir.close();

        } catch (FileNotFoundException fe) {
            System.out.println("File not found!");
            throw fe;
        } catch (IOException ie) {
            System.out.println("IO Error");
            throw ie;
        }

    }


    public String getType(){
        return this.type;
    }

    public String getContent(){
        return new String(this.content);
    }

    public String getPath(){
        return this.path;
    }

    public String[] getFileContent(){
        return fileContent.split("\\x00");
    }

    public byte[] getRealContent() {
        return this.content;
    }

    public int getLength() {
        return this.length;
    }

    public String getHash(){
        return this.hash;
    }

}