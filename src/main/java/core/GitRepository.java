package core;

import java.io.*;

public class GitRepository{

    private String repoPath;

    public GitRepository(String repoPath){
        this.repoPath = repoPath;
    }

    public String getHeadRef() throws IOException{
        // Read the file
        String headFileContent = readFile(this.repoPath + File.separator + "HEAD");

        return headFileContent.split(" ")[1];
    }

    public String getRefHash(String path) throws IOException{
        // Read the file
        String refContent = readFile(this.repoPath + File.separator + path);

        return refContent;
    }

    public String readFile(String path) throws FileNotFoundException, IOException{
        String output = "";
        String line = "";

        File file = new File(path);

        BufferedReader br = new BufferedReader(new FileReader(file));

        while ((line = br.readLine()) != null) {
            output = line;
        }

        return output;
    }
}