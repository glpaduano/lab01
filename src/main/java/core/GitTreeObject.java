package core;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class GitTreeObject extends GitBlobObject{

    /*
    The commit object has some special fields on addition to the normal
    blob type. Qualify them properly.
    */
    private HashMap<String, GitBlobObject> elements = new HashMap<String,GitBlobObject>();

    //Constructor
    public GitTreeObject(String path, String hash) throws IOException {

        // Create a blob object...
        super(path, hash);

        /*
        Used this link to understand the tree structure http://stackoverflow.com/a/21599232

        Structure:

        tree [content size]\0[Entries having references to other trees and blobs]

        [mode] [file/folder name]\0[SHA-1 of referencing blob or tree]

        To find out if the element is a file or not, we can check the first
        byte and it should be equal to the character '1' <---
        (it's really important that it is a character!)
        */
        byte b = '0', startHash = '\0', startName = ' ', fileByte = '1';
        StringBuffer sb = new StringBuffer();
        String read_name, read_hash;
        byte[] content = this.getRealContent();
        boolean check=true, is_file=true;

        // Check the blob for stuff and create the map!
        for (int i = 0; i < this.getLength(); i++) {
            // Read the byte
            b = content[i];

            if (check) {
                // At the first byte and after each SHA1, we should check
                // if the element we have is a file or not.
                is_file = (b == fileByte);

                check = false;
            } else if (b == startName) {
                // Reset the string buffer, drop the modal code inside!
                sb = new StringBuffer();
            } else if (b == startHash) {
                // We have the name in the string buffer already
                // We have a whitespace stored in memory to skip
                read_name = sb.toString().substring(1);

                // Read the hash
                sb = new StringBuffer();

                for (int j=0; j < 20; j++)
                    // Formatting properly the string: http://stackoverflow.com/a/20897686
                    sb.append(String.format("%02x", content[i+j+1]));

                i += 20;
                read_hash = sb.toString();

                // Add the element in the hash map
                elements.put(read_name, is_file ?
                        new GitBlobObject(path, read_hash) :
                        new GitTreeObject(path, read_hash)
                );

                // Now, the next thing to check is the next file type
                check = true;
            }

            // Store the read character in the StringBuffer
            sb.append((char) b);
        }
    }

    
    public Set<String> getEntryPaths() {
        return elements.keySet();
    }

    public GitBlobObject getEntry(String name) {
        return elements.get(name);
    }
}
